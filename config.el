git clone --depth 1 https://github.com/hlissner/doom-emacs ~/.config/emacs
~/.config/emacs/bin/doom install

git clone https://gitlab.com/KisuYami/doomconfig ~/.config/doom
~/.config/emacs/bin/doom sync

(add-to-list 'default-frame-alist '(fullscreen . maximized))

(setq doom-theme 'doom-monokai-classic)

(setq diary-file "~/Documents/Org/Agenda/diary")
(setq org-agenda-files (quote ("~/Documents/Org/Agenda")))

(map! :leader
      :desc "Toggle Modeline"
      "t m" #'hide-mode-line-mode)

(map! :leader
      :desc "Find definition at point"
      "]" #'xref-find-definitions)

(map! :leader
      :desc "Find all references"
      "[" #'xref-find-references)
