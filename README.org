* Install
** Install Doom Emacs
#+begin_src bash
git clone --depth 1 https://github.com/hlissner/doom-emacs ~/.config/emacs
~/.config/emacs/bin/doom install
#+end_src
** Configuration Files
#+begin_src bash
git clone https://gitlab.com/KisuYami/doomconfig ~/.config/doom
~/.config/emacs/bin/doom sync
#+end_src

* Display
** Start in Full Screen
#+begin_src emacs-lisp
(add-to-list 'default-frame-alist '(fullscreen . maximized))
#+end_src
** Theme
#+begin_src emacs-lisp
(setq doom-theme 'doom-monokai-classic)
#+end_src
* Org
** Agenda & Diary
#+BEGIN_SRC emacs-lisp
(setq diary-file "~/Documents/Org/Agenda/diary")
(setq org-agenda-files (quote ("~/Documents/Org/Agenda")))
#+END_SRC
* Keybinds
** UI
#+begin_src emacs-lisp
(map! :leader
      :desc "Toggle Modeline"
      "t m" #'hide-mode-line-mode)
#+end_src
** Navigation
#+begin_src emacs-lisp
(map! :leader
      :desc "Find definition at point"
      "]" #'xref-find-definitions)

(map! :leader
      :desc "Find all references"
      "[" #'xref-find-references)
#+end_src
